//
//  TutorialViewController.m
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "TutorialViewController.h"
#import "TutorialContentViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "lightGreenTheme.h"

@interface TutorialViewController ()
@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) NSArray *pageImages;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@end

@implementation TutorialViewController

#pragma mark - getter & setter
- (NSArray *)pageImages
{
    return @[
             @"tutorial_0_scan.png",
             @"tutorial_1_list.png",
             @"tutorial_2_detail.png"
             ];
}

- (UIPageViewController *)pageViewController
{
    if (!_pageViewController) {
        _pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialPageViewController"];
        _pageViewController.dataSource = self;
    }
    return _pageViewController;
}

#pragma mark - view controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    TutorialContentViewController *startingViewController = [self viewControllerAtIndex: 0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:YES
                                     completion:nil];
    
    const int BOTTOM_BOTTOM_HEIGHT = 60;
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - BOTTOM_BOTTOM_HEIGHT);
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blueColor];
    
    // set up button
    self.startButton.backgroundColor = [lightGreenTheme buttonBackgroundColor];
    self.startButton.tintColor = [lightGreenTheme buttonTitleColor];
    self.startButton.layer.cornerRadius = [lightGreenTheme buttonCornerRadius];
    //self.startButton.titleLabel.tintColor = [lightGreenTheme buttonTitleColor];
    self.startButton.titleLabel.font = [UIFont fontWithName:[lightGreenTheme buttonFont]
                                                       size:16.0f];
}

#pragma mark - helper function
- (TutorialContentViewController *)viewControllerAtIndex: (NSUInteger)index
{
    if (0 == self.pageImages.count || index > self.pageImages.count) {
        return nil;
    }
    
    TutorialContentViewController *contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    contentViewController.pageIdx = index;
    contentViewController.imgFileName = self.pageImages[index];
    
    return contentViewController;
}

#pragma mark - delegation
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((TutorialContentViewController *) viewController).pageIdx;
    if (0 == index || NSNotFound == index) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex: index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((TutorialContentViewController *) viewController).pageIdx;
    
    if (NSNotFound == index) {
        return nil;
    }
    
    index++;
    if (index == self.pageImages.count) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return self.pageImages.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

#pragma mark - button action
- (IBAction)startNow:(UIButton *)sender
{
    [self.view.superview removeFromSuperview];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.entryPage = ENTRY_QR;
}

@end
