//
//  OutputInvoiceTableViewCell.m
//  invoiceQR
//
//  Created by Hank  on 2016/1/18.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "OutputInvoiceTableViewCell.h"
#import "lightGreenTheme.h"

@implementation OutputInvoiceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code    
    self.dateLabel.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                          size:[lightGreenTheme labelFontSize]];
    self.invoiceLabel.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                               size:[lightGreenTheme labelFontSize]];
    self.buyerVAT.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                           size:[lightGreenTheme labelFontSize]];
    self.sellerVAT.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                                size:[lightGreenTheme labelFontSize]];
    self.sellMoneyLabel.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                             size:[lightGreenTheme labelFontSize]];
    self.taxLabel.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                         size:[lightGreenTheme labelFontSize]];
    self.totalMoneyLabel.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                          size:[lightGreenTheme labelFontSize]];
}

@end
