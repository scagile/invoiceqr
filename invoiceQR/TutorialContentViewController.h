//
//  TutorialContentViewController.h
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialContentViewController : UIViewController
@property (nonatomic) NSUInteger pageIdx;
@property (nonatomic, strong) NSString *imgFileName;
@end
