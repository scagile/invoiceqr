//
//  ListTableViewController.m
//  invoiceQR
//
//  Created by Hank  on 2016/1/16.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "ListTableViewController.h"
#import "Utility.h"
#import "Constants.h"
#import "OutputInvoiceTableViewCell.h"
#import "AppDelegate.h"
#import "InvoiceDetailViewController.h"
#import "Items.h"
#import "lightGreenTheme.h"

@import AVFoundation;

@interface ListTableViewController ()
@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *invoiceFectchResultController;
@property (strong, nonatomic) Invoice *selectedInvoice;

@property (strong, nonatomic) UISearchController *searchController;

@property (strong, nonatomic) NSArray *searchResults;
@end

@implementation ListTableViewController

#pragma mark - view controller lifecycle
- (void)viewDidLoad
{
    [self initController];
    [self initInterface];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Invoice"
                                              inManagedObjectContext:self.managedObjectContext];
    NSSortDescriptor *dateSort = [[NSSortDescriptor alloc]initWithKey:@"date" ascending:YES];
    NSSortDescriptor *vatSort = [[NSSortDescriptor alloc] initWithKey:@"buy_vat_no" ascending:YES];
    fetchRequest.sortDescriptors = @[vatSort, dateSort];
    [fetchRequest setEntity:entity];
    
    self.invoiceFectchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                             managedObjectContext:self.managedObjectContext
                                                                               sectionNameKeyPath:@"buy_vat_no"
                                                                                        cacheName:@"invoiceCache"];
    
    self.invoiceFectchResultController.delegate = self;
    
    NSError *fetchError = nil;
    if ([self.invoiceFectchResultController performFetch:&fetchError]) {
        NSLog(@"Successfully fetched data from invoce entity");
    }else{
        NSLog(@"Failed to fetch any data from invoice entity");
    }
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
}

#pragma mark - init
- (void)initController
{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    // the new delegate in iOS 8
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    //self.searchController.searchBar.scopeButtonTitles = @[NSLocalizedString(@"ScopeButtonVAT", @"VAT"), NSLocalizedString(@"ScopeButtonDate", @"Date")];
    self.searchController.searchBar.delegate = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    [self.searchController.searchBar sizeToFit];
}

- (void)initInterface
{
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    //[[UIView appearanceWhenContainedInInstancesOfClasses:@[[UITabBar class]]] setTintColor:[lightGreenTheme naviagtionBackgroundColor]];
#if 0
    for(NSUInteger i=0; i< self.tabBarController.tabBar.items.count; ++i){
        if (i != self.tabBarController.selectedIndex) {
            UITabBarItem *tabItem = [self.tabBarController.tabBar.items objectAtIndex:i];
            tabItem.image = [tabItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            tabItem.selectedImage = [tabItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
    }
#endif
}

#pragma mark - setter & getter
- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext = appDelegate.managedObjectContext;
    }
    return _managedObjectContext;
}

#pragma mark - delegate - fetch controller delegate
- (void)controllerDidChangeContent: (NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
}

#pragma mark - delegate - table view delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.searchController.active && ![self.searchController.searchBar.text isEqual: @""]) {
        return 1;
    }
    return [[self.invoiceFectchResultController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.searchController.active && ![self.searchController.searchBar.text isEqual: @""])
    {
        return [self.searchResults count];
    }
    
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.invoiceFectchResultController.sections objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OUTPUT_TAX_INVOICE_CELL";
    
    OutputInvoiceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[OutputInvoiceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Invoice *invoice = nil;
    
    if (self.searchController.active && ![self.searchController.searchBar.text isEqual: @""]) {
        invoice = [self.searchResults objectAtIndex:indexPath.row];
    }else{
        invoice = [self.invoiceFectchResultController objectAtIndexPath:indexPath];
    }

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    cell.dateLabel.text = [formatter stringFromDate:invoice.date];
    
    NSMutableString *invoiceStr = [NSMutableString stringWithString:invoice.invoice_num];
    [invoiceStr insertString:@"-" atIndex:2];
    cell.invoiceLabel.text = [invoiceStr copy];
    
    cell.buyerVAT.text = invoice.buy_vat_no;
    cell.sellerVAT.text = invoice.sell_vat_no;
    cell.sellMoneyLabel.text = [invoice.sale_money stringValue];
    cell.totalMoneyLabel.text = [invoice.total_money stringValue];
    cell.taxLabel.text = (invoice.tax_money > 0)? [invoice.tax_money stringValue] : @"0";

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (self.searchController.active && ![self.searchController.searchBar.text isEqual: @""]) {
        self.selectedInvoice = [self.searchResults objectAtIndex:indexPath.row];
    }else{
        self.selectedInvoice = [self.invoiceFectchResultController objectAtIndexPath:indexPath];
    }
    
    [self performSegueWithIdentifier:@"INVOICE_DETAIL_SEGUE" sender:self.view];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Invoice *invoice = [self.invoiceFectchResultController objectAtIndexPath:indexPath];
        NSError *error = nil;
        [self.managedObjectContext deleteObject:invoice];
        if ([self.managedObjectContext save:&error]){
            NSLog(@"invoice was deleted");
        }else {
            NSLog(@"Failed to delete the invoice, Error = %@", error);
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.invoiceFectchResultController.sections objectAtIndex:section];
    return [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"buyer", @"buyer"), [sectionInfo name]];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    header.textLabel.textColor = [UIColor blackColor];
    header.textLabel.font = [UIFont boldSystemFontOfSize:16];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
}

#if ENABLE_MOVE_ACTION
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSMutableArray *things = [[self.invoiceFectchResultController fetchedObjects] mutableCopy];
    NSManagedObject *thing = [self.invoiceFectchResultController objectAtIndexPath:sourceIndexPath];
    
    [things removeObject:thing];
    [things insertObject:thing atIndex:[destinationIndexPath row]];
    
    // All of the objects are now in their correct order. Update each
    // object's displayOrder field by iterating through the array.
    int i = 0;
    for (NSManagedObject *mo in things)
    {
        [mo setValue:[NSNumber numberWithInt:i++] forKey:@"index"];
    }
    
    [self.managedObjectContext save:nil];
}
#endif

#pragma mark - delegate search controller
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self filterContentFromSearch:searchController.searchBar.text scope:@"ALL"];
    [self.tableView reloadData];
}

#pragma mark - helper functions
- (void)filterContentFromSearch: (NSString *)searchText
                          scope: (NSString *)scope
{
#if 1
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY items.name contains[cd] %@", searchText];
#else
    NSDateFormatter *mmddccyy = [[NSDateFormatter alloc] init];
    mmddccyy.timeStyle = NSDateFormatterNoStyle;
    mmddccyy.dateFormat = @"MM/dd/yyyy";
    NSDate *startDate = [mmddccyy dateFromString:@"02/01/2016"];
    NSDate *endDate = [mmddccyy dateFromString:@"03/01/2016"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(date >= %@) AND (date <= %@)", startDate, endDate];
#endif
    
    if([@"" isEqualToString:searchText]){
        self.searchResults = [[self.invoiceFectchResultController fetchedObjects] copy];
    }else{
        for (Invoice *invoice in [self.invoiceFectchResultController fetchedObjects]){
            for ( Items *item in invoice.items) {
                NSLog(@"%@", item.name);
            }
        }

        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"date"
                                                                     ascending:YES];
        self.searchResults = [[self.invoiceFectchResultController fetchedObjects] filteredArrayUsingPredicate:predicate];
        [self.searchResults sortedArrayUsingDescriptors:@[descriptor]];
    }
}

- (NSString *)exportVisibleResults
{
    // header
    NSMutableString *exportStr = [NSMutableString stringWithFormat:@"%@, %@, %@, %@, %@\n",
                                //NSLocalizedString(@"發票日期", @"invoice date"),
                                @"發票日期",
                                @"發票號碼",
                                @"賣家統編",
                                @"銷售金額",
                                @"營業稅額"
                                ];
    
#if EXPORT_ONLY_VISIBLE
    NSArray *cells = [self.tableView visibleCells];
    
    for (OutputInvoiceTableViewCell *cell in cells) {
        if ([cell isKindOfClass:[OutputInvoiceTableViewCell class]]) {
            [exportStr appendFormat:@"%@, %@, \"%@\", %@, %@\n",
                         cell.dateLabel.text,
                         cell.invoiceLabel.text,
                         cell.sellerVAT.text,
                         cell.sellMoneyLabel.text,
                         cell.taxLabel.text];
        }
    }
#else
    self.searchResults = [[self.invoiceFectchResultController fetchedObjects] copy];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    
    for (Invoice *invoice in self.searchResults){
        [exportStr appendFormat:@"%@, %@,=\"%@\", %@, %@\n",
         [formatter stringFromDate:invoice.date],
         [NSMutableString stringWithString:invoice.invoice_num],
         invoice.sell_vat_no,
         [invoice.sale_money stringValue],
         (invoice.tax_money > 0)? [invoice.tax_money stringValue] : @"0"];
    }
#endif
    // remove last ,
    // NSString *exportStr = [tempStr substringToIndex:[tempStr length]-1];
    return [exportStr copy];
}

#pragma mark - button actions
- (IBAction)exportData:(UIBarButtonItem *)sender
{
    // fixme later, use itemProvider to prepare data
    // http://stackoverflow.com/questions/20581389/how-do-i-use-uiactivityitemprovider-to-send-an-email-with-attachment-with-uiacti
    
    // 1. cell to csv string
    NSString *csvString = [self exportVisibleResults];
    
    // 2. csv string to CSV file
    NSData *csvData = [csvString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString* docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* csvFilePath = [docPath stringByAppendingPathComponent: @"invoice.csv"];
    [csvData writeToFile:csvFilePath atomically:YES];
    NSURL *csvFileUrl = [NSURL fileURLWithPath:csvFilePath];
    
    NSString *texttoshare = NSLocalizedString(@"Attached file contains invoice info in csv format.", @"Attached description");
    // you can add more decoration
#if ADD_MORE_RESOURCE
    UIImage *imagetoshare = [UIImage imageNamed:@"barcode.png"];
    if (imagetoshare != nil) {
        activityItems = @[imagetoshare, texttoshare, csvFileUrl];
    } else {
        activityItems = @[texttoshare, csvFileUrl];
    }
#endif
    
    NSArray *activityItems;
    activityItems = @[texttoshare, csvFileUrl];
    
    NSArray *exTypes = @[UIActivityTypePostToWeibo, UIActivityTypePrint,
                         UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact,
                         UIActivityTypeSaveToCameraRoll, UIActivityTypeAddToReadingList,
                         UIActivityTypePrint, UIActivityTypePostToFlickr,
                         UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo
                        ];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                                                                     applicationActivities:nil];
    activityController.excludedActivityTypes = exTypes;
    
    [activityController setValue: NSLocalizedString(@"Export invoice data", @"export mail subject") forKey:@"subject"];
    [self presentViewController:activityController animated:YES completion:nil];
}


#pragma mark - navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"INVOICE_DETAIL_SEGUE"]) {
        InvoiceDetailViewController *invoiceDetailVC = segue.destinationViewController;
        invoiceDetailVC.invoiceID = [self.selectedInvoice objectID];
        invoiceDetailVC.invoiceNum = self.selectedInvoice.invoice_num;
    }
}
@end
