//
//  Items+CoreDataProperties.h
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Items.h"

NS_ASSUME_NONNULL_BEGIN

@interface Items (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *count;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *totalPrice;
@property (nullable, nonatomic, retain) NSNumber *unitPrice;
@property (nullable, nonatomic, retain) NSDate *soldDate;
@property (nullable, nonatomic, retain) NSString *companyName;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) Invoice *belongInvoice;

@end

NS_ASSUME_NONNULL_END
