//
//  RootNaviViewController.m
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "RootNaviViewController.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface RootNaviViewController ()
@property (strong, nonatomic) AppDelegate *appDelegate;
@end

@implementation RootNaviViewController

#pragma mark - init
- (AppDelegate *)appDelegate
{
    if (nil == _appDelegate) {
        _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

#pragma mark - view controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [self navigateToPage: self.appDelegate.entryPage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - helper functions
- (void)navigateToPage: (enum PROGRAM_ENTRY) programEntry
{
    [self performSegueWithIdentifier:PROGRAM_ENTRY_NAME[programEntry] sender:self.view];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
