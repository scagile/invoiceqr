//
//  InvoiceTabBarViewController.m
//  invoiceQR
//
//  Created by Hank  on 2016/4/11.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "InvoiceTabBarViewController.h"
#import "Constants.h"
#import "lightGreenTheme.h"

@interface InvoiceTabBarViewController ()

@end

@implementation InvoiceTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger entrance = [userDefaults integerForKey: KEY_NAME[KEY_ENTRANCE]];

    /*
    for(UITabBarItem *tabItem in self.tabBar.items){
        tabItem.image = [tabItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        //tabItem.image = [tabItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabItem.selectedImage = [tabItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
     */

    //[[UIView appearanceWhenContainedInInstancesOfClasses:@[[UITabBar class]]] setTintColor:[lightGreenTheme naviagtionBackgroundColor]];
    
    self.selectedIndex = entrance;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
