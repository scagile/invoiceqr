//
//  Constants.m
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

NSString *const PROGRAM_ENTRY_NAME[ENTRY_ALL] =
{
    @"TUTORIAL",
    @"QR"
};

NSString *const KEY_NAME[KEY_ALL] =
{
    @"ENABLE_TUTORIAL",
    @"ENABLE_CONTINUE_SCAN",
    @"ENTRANCE"
};