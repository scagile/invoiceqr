//
//  Utility.m
//  invoiceQR
//
//  Created by Hank  on 2016/1/17.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "Utility.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "Items.h"
#import "Users.h"

@import AVFoundation;

@interface Utility()
@end

@implementation Utility

static NSString *utility_coding=@"";

+ (NSMutableArray *)parseRightQRfromString: (NSString *)rightQR
{
    NSMutableArray *data = [[NSMutableArray alloc] init];
    
    rightQR = [rightQR stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    const int LENGTH_OF_COMMA = 1;
    if ([rightQR hasSuffix:@":"]) {
        rightQR = [rightQR substringToIndex: [rightQR length] - LENGTH_OF_COMMA];
    }
    
    const int LENGTH_OF_DOUBLE_STAR = 2;
    if ([rightQR hasPrefix:@"**"]) {
        rightQR = [rightQR substringFromIndex: LENGTH_OF_DOUBLE_STAR];
    }
    
    NSMutableArray *leftData = [[rightQR componentsSeparatedByString:@":"] mutableCopy];
    const NSString *BIG5_CODING = @"0";
    
    for (size_t i=0; i<[leftData count]; i++) {
        if ([BIG5_CODING isEqualToString: utility_coding]) {
            if ([[leftData[i] class] isSubclassOfClass:[NSString class]]) {
                NSStringEncoding big5 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5);
                NSData *rawData = [leftData[i] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSMutableData *processedData = [[NSMutableData alloc]init];
                const char *bytes = [rawData bytes];
                for (int i = 0; i < [rawData length]; i++)
                {
                    if (i+1 < [rawData length]) {
                        if ('\xef' == bytes[i] && '\xbd' == bytes[i+1]) {
                            i = i+1;
                            continue;
                        }
                    }
                    [processedData appendBytes:(const char *) &bytes[i] length:1];
                }
                NSString *refinedData = [[NSString alloc] initWithData:processedData encoding:big5];
                if(refinedData) [leftData replaceObjectAtIndex:i withObject:refinedData];
            }
        }
        [data addObject: [leftData[i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    return [data copy];
}

+ (NSMutableArray *)parseInvoiceFromString: (NSString *)invoiceString
{
    NSMutableArray *data = [[NSMutableArray alloc] init];

    const int LENGTH_OF_COMMA = 1;
    if ([invoiceString hasSuffix:@":"]) {
        invoiceString = [invoiceString substringToIndex: [invoiceString length] - LENGTH_OF_COMMA];
    }
    
    const int INVOICE_HEADER_LENGTH = 89;
    if (invoiceString.length > INVOICE_HEADER_LENGTH) {

        NSString *number = [invoiceString substringWithRange: NSMakeRange(0, 10)];
        [data addObject:number];
    
        NSString *date = [invoiceString substringWithRange:NSMakeRange(10, 7)];
        [data addObject:date];
    
        NSString *randomNum = [invoiceString substringWithRange:NSMakeRange(17, 4)];
        [data addObject:randomNum];
    
        NSString *saleMoney = [invoiceString substringWithRange:NSMakeRange(21, 8)];
        [data addObject:saleMoney];

        NSString *totalMoney = [invoiceString substringWithRange:NSMakeRange(29, 8)];
        [data addObject:totalMoney];
    
        NSString *buyerVAT = [invoiceString substringWithRange:NSMakeRange(37, 8)];
        [data addObject:buyerVAT];

        NSString *sellerVAT = [invoiceString substringWithRange:NSMakeRange(45, 8)];
        [data addObject:sellerVAT];
    
        NSString *encryptInfo = [invoiceString substringWithRange:NSMakeRange(53, 24)];
        [data addObject:encryptInfo];
        
        // digit 77 will be :
        NSString *otherInfo = [invoiceString substringWithRange:NSMakeRange(78, 10)];
        [data addObject:otherInfo];
        
        // digit 88 should be :
        NSString *leftStrings = [invoiceString substringFromIndex:89];
        NSMutableArray *leftData = [[leftStrings componentsSeparatedByString:@":"] mutableCopy];
        
        NSString *qrCodeItemCount = leftData[0];
        [data addObject:qrCodeItemCount];
        
        NSString *totalItemCount = leftData[1];
        [data addObject:totalItemCount];
        
        NSString *coding = leftData[2];
        utility_coding = [NSString stringWithString:coding];
        const NSString *BIG5_CODING = @"0";
        //const NSString *UTF8_COIDING = @"1";
        //const NSString *BASE64_CODING = @"2";
        
        [NSString stringWithString:coding];
        
        if ([coding length] >= 2) {
            for (size_t i=2; i<[leftData count]; i++) {
                [data addObject: [leftData[i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            }
        }else{
            [data addObject:coding];
            
            for (size_t i=3; i<[leftData count]; i++) {
                if ([BIG5_CODING isEqualToString:coding]) {
                    if ([[leftData[i] class] isSubclassOfClass:[NSString class]]) {
                        NSStringEncoding big5 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5);
                        NSData *rawData = [leftData[i] dataUsingEncoding:NSUTF8StringEncoding];
                        
                        NSMutableData *processedData = [[NSMutableData alloc]init];
                        const char *bytes = [rawData bytes];
                        for (int i = 0; i < [rawData length]; i++)
                        {
                            if (i+1 < [rawData length]) {
                                if ('\xef' == bytes[i] && '\xbd' == bytes[i+1]) {
                                    i = i+1;
                                    continue;
                                }
                            }
                            [processedData appendBytes:(const char *) &bytes[i] length:1];
                        }
                        NSString *refinedData = [[[NSString alloc] initWithData:processedData encoding:big5 ] stringByRemovingPercentEncoding];
                        if(refinedData) {
                            [leftData replaceObjectAtIndex:i withObject:refinedData];
                        }
                    }
                }
                [data addObject: [leftData[i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            }
        }
    }
    return [data copy];
}

+ (void)fetchInvoiceDetailwithNum: (Invoice *)invoice
                          Context: (NSManagedObjectContext *)managedObjectContext
{
     NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
     AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
     AFJSONResponseSerializer *jsonResponseSerializer = [AFJSONResponseSerializer serializer];
     jsonResponseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
     manager.responseSerializer = jsonResponseSerializer;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    
#if QUERY_ONLY_HEADER
     NSDictionary *parameters = @{
        @"version" : @"0.2",
        @"type" : @"Barcode",
        @"invNum" : invoice.invoice_num,
        @"action" : @"qryInvHeader",
        @"generation" : @"V2",
        @"invDate" : [dateFormatter stringFromDate:invoice.date],
        @"appID" : @"EINV0201603168128",
        @"UUID" : @"b23563fb041452ea4d60666a2aae12a1ccb33873"
    };
#else
    NSDictionary *parameters = @{
         @"version" : @"0.2",
         @"type" : @"QRCode",
         @"invNum" : invoice.invoice_num,
         @"action" : @"qryInvDetail",
         @"generation" : @"V2",
         @"invTerm" : invoice.period,
         @"randomNumber" : invoice.randomNum,
         @"invDate" : [dateFormatter stringFromDate:invoice.date],
         @"sellerID" : invoice.sell_vat_no,
         @"encrypt" : invoice.encryp,
         @"appID" : @"EINV0201603168128",
         @"UUID" : @"b23563fb041452ea4d60666a2aae12a1ccb33873"
         };
#endif
    
    const NSString *GOV_E_INVOICE_URL_PREFIX = @"https://www.einvoice.nat.gov.tw/PB2CAPIVAN/invapp/InvApp";
     
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET"
                                                                           URLString: [NSString stringWithFormat:@"%@", GOV_E_INVOICE_URL_PREFIX]
                                                                          parameters:parameters error:nil];
    
    NSURLSessionDataTask *dataTask =
        [manager dataTaskWithRequest:request
                   completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                       if (error) {
                           NSLog(@"Error: %@", error);
                       } else {
                           NSLog(@"%@ %@", response, responseObject);
                           Users *newCompnay = [NSEntityDescription insertNewObjectForEntityForName:@"Users"
                                                                             inManagedObjectContext:managedObjectContext];
                           if (!newCompnay) {
                               NSLog(@"Failed to create new company info");
                           }
                           newCompnay.vat_num = invoice.invoice_num;
                           newCompnay.name = [responseObject objectForKey:@"sellerName"];
                           
                           NSFetchRequest *request = [[NSFetchRequest alloc] init];
                           request.entity = [NSEntityDescription entityForName:@"Items" inManagedObjectContext:managedObjectContext];
                           NSPredicate *predicate = [NSPredicate predicateWithFormat:@"belongInvoice.invoice_num contains %@", invoice.invoice_num];
                           request.predicate = predicate;
                           NSError *error = nil;
                           NSArray *objs = [managedObjectContext executeFetchRequest:request error:&error];

                           // replacement policy, remove all first
                           for (NSManagedObject *object in objs) {
                               [managedObjectContext deleteObject:object];
                           }
                           
                           if ([managedObjectContext save:&error]){
                                NSLog(@"old data were deleted");
                           }else {
                                NSLog(@"Failed to delete the items, Error = %@", error);
                           }
                           
                           NSArray *invoiceItems = [responseObject objectForKey:@"details"];
                           for (int i = 0; i<invoiceItems.count; ++i) {
                               Items *newItem = [NSEntityDescription insertNewObjectForEntityForName:@"Items"
                                                                              inManagedObjectContext:managedObjectContext];
                               
                               if (!newItem) { NSLog(@"Failed to create new item"); }
                               newItem.name = [invoiceItems[i] objectForKey:@"description"];
                               
                               NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                               f.numberStyle = NSNumberFormatterDecimalStyle;
                               newItem.count = [f numberFromString: [invoiceItems[i] objectForKey:@"quantity"]];
                               newItem.totalPrice = [f numberFromString: [invoiceItems[i] objectForKey: @"amount"]];
                               newItem.unitPrice = [f numberFromString: [invoiceItems[i] objectForKey:@"unitPrice"]];
                               newItem.belongInvoice = invoice;
                               newItem.soldDate = invoice.date;
                               newItem.companyName = invoice.seller.name;
                               
                               NSError *saveErr = nil;
                               if ([managedObjectContext save:&saveErr]) {
                                   NSLog(@"[Utility] - save new item");
                               }else{
                                   NSLog(@"[Utility] - save item failed");
                               }
                           }
                           
                           invoice.seller = newCompnay;
                           newCompnay.lastSync = [NSDate date];
                           newCompnay.lastUpdate = [NSDate date];
                           
                           NSError *saveErr = nil;
                           if ([managedObjectContext save:&saveErr]) {
                               NSLog(@"[Utility] - save new coompany");
                           }else{
                               NSLog(@"[Utility] - save company failed");
                           }
                       }
                   }];
    [dataTask resume];
}

+ (enum E_ERROR_CODE)getInvoiceFromAVMetaDataList:(NSArray *)invoiceLists
                 save2ManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
                            withInvoiceNum:(NSString **)invoiceNum
{
    const int TW_INVOICE_QR_COUNT = 2;
    Invoice *newInvoice = nil;
    
    if ( TW_INVOICE_QR_COUNT == invoiceLists.count ) {
        AVMetadataObject *firstObj = [invoiceLists firstObject];
        AVMetadataObject *lastObj = [invoiceLists lastObject];
        
        if ([firstObj.type isEqualToString:AVMetadataObjectTypeQRCode] &&
            [lastObj.type isEqualToString:AVMetadataObjectTypeQRCode])
        {
            AVMetadataMachineReadableCodeObject *leftInvoiceObj=nil, *rightInvoiceObj=nil;
            
            if (firstObj.bounds.origin.x >= lastObj.bounds.origin.x) {
                leftInvoiceObj = (AVMetadataMachineReadableCodeObject *)lastObj;
                rightInvoiceObj = (AVMetadataMachineReadableCodeObject *)firstObj;
            }else{
                leftInvoiceObj = (AVMetadataMachineReadableCodeObject *)firstObj;
                rightInvoiceObj = (AVMetadataMachineReadableCodeObject *)lastObj;
            }
            
            NSString *leftContents = leftInvoiceObj.stringValue? leftInvoiceObj.stringValue : @"Unable to decode";
            NSString *rightContents = rightInvoiceObj.stringValue? rightInvoiceObj.stringValue : @"Unable to decode";
            
            NSMutableArray *parsedLeftQR = [self parseInvoiceFromString:leftContents];
            NSMutableArray *parsedRightQR = [self parseRightQRfromString:rightContents];
            
            //NSStringEncoding big5 = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5_HKSCS_1999);
            //NSString *str = [[NSString alloc] initWithData:input encoding:big5];
            
            //rightContents = [rightContents stringByAddingPercentEncodingWithAllowedCharacters:kCFStringEncodingUTF8];
            //rightContents = [rightContents stringByAddingPercentEscapesUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingUTF8)];
            //NSMutableArray *parsedRightQR = [self parseInvoiceFromString:rightContents];
            
            NSLog(@"%@", parsedLeftQR);
            // utf8 to big5
            // stringValue = [stringValue stringByAddingPercentEscapesUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5)];
            
            // big5 to utf8
            // stringValue = [stringValue stringByAddingPercentEscapesUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingUTF8)];
            
            if(parsedLeftQR != nil && parsedLeftQR.count > 0 ){
                NSMutableArray *takenQR = parsedLeftQR;
                
                NSFetchRequest *request = [[NSFetchRequest alloc] init];
                request.entity = [NSEntityDescription entityForName:@"Invoice" inManagedObjectContext:managedObjectContext];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"invoice_num contains %@", takenQR[INVOICE_NUM]];
                request.predicate = predicate;
                NSError *error = nil;
                NSArray *objs = [managedObjectContext executeFetchRequest:request error:&error];
                if (error) {
                    [NSException raise:@"no invoice find" format:@"%@", [error localizedDescription]];
                }
                if (objs.count > 0) {
                    // there is an invoice with same invoice number exsist. Use update method
                    Invoice *invoice = [objs objectAtIndex:0];
                    [NSString stringWithFormat:@"%@ - duplicate", invoice.invoice_num];
                    *invoiceNum = [invoice.invoice_num copy];
                    return E_ERROR_DUPLICATE_INVOICE;
                }else {
                    newInvoice = [NSEntityDescription insertNewObjectForEntityForName:@"Invoice"
                                                               inManagedObjectContext:managedObjectContext];
                    if (!newInvoice) {
                        NSLog(@"Failed to create new invoice");
                    }
                }

                newInvoice.invoice_num = takenQR[INVOICE_NUM];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.timeStyle = NSDateFormatterNoStyle;
                dateFormatter.dateFormat = @"yyyyMMdd";
                //dateFormatter.dateFormat = @"MM/dd/yyyy";
                //NSDate *d = [mmddccyy dateFromString:@"12/11/2005"];
                // it's in Taiwan calendar, e.g. 20160128 => 1050128
                int dateInInt = [takenQR[INVOICE_DATE] intValue];
                dateInInt += 19110000;
                newInvoice.date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%d", dateInInt]];
                
                const int MONTH_LENGTH = 2;
                int period = [[takenQR[INVOICE_DATE] substringToIndex:[takenQR[INVOICE_DATE] length]- MONTH_LENGTH] intValue];
                if( 1 == period % 2) period += 1;
                newInvoice.period = [NSString stringWithFormat:@"%d", period];
                newInvoice.sell_vat_no = takenQR[INVOICE_SELL_VAT_NO];
                newInvoice.buy_vat_no = takenQR[INVOICE_BUY_VAT_NO];
                newInvoice.randomNum = takenQR[INVOICE_RANDOM];
                newInvoice.encryp = takenQR[INVOICE_ENCRY];
                newInvoice.lastUpdate = [NSDate date];
                
                // Check company info exists
                request.entity = [NSEntityDescription entityForName:@"Users" inManagedObjectContext:managedObjectContext];
                predicate = [NSPredicate predicateWithFormat:@"vat_num contains %@", newInvoice.invoice_num];
                request.predicate = predicate;
                objs = [managedObjectContext executeFetchRequest:request error:&error];
                if (error) {
                    [NSException raise:@"no company found" format:@"%@", [error localizedDescription]];
                }
                
                if (objs.count > 0) {
                    newInvoice.seller = [objs objectAtIndex:0];
                }else{
                    [Utility fetchInvoiceDetailwithNum:newInvoice
                                               Context:managedObjectContext ];
                }
                
                NSString *total_moeny_hex = takenQR[INVOICE_TOTAL_MOENY_HEX];
                NSScanner *pScanner = [NSScanner scannerWithString: total_moeny_hex];
                
                unsigned long long totalVal;
                [pScanner scanHexLongLong: &totalVal];
                newInvoice.total_money = [NSNumber numberWithLongLong:totalVal];
                
                NSString *sell_money_hex = takenQR[INVOICE_SALE_MONEY_HEX];
                const float SELL_TAX_RATIO = 0.05f;
                const float ROUND_VAL = 0.5f;
                if(![@"00000000" isEqualToString:sell_money_hex]){
                    pScanner = [NSScanner scannerWithString: sell_money_hex];
                    
                    unsigned long long moneyVal;
                    [pScanner scanHexLongLong: &moneyVal];
                    newInvoice.sale_money = [NSNumber numberWithLongLong:moneyVal];
                    
                    unsigned long long taxVal = totalVal - moneyVal;
                    newInvoice.tax_money = [NSNumber numberWithLongLong:taxVal];
                    
                    // validation!! There are lots of error invoices
                    int totalMoney = [newInvoice.total_money intValue];
                    int tax = [newInvoice.tax_money intValue];
                    
                    const float ERROR_TORLENCE = 0.02f;
                    
                    if(fabsf((float)tax/totalMoney - SELL_TAX_RATIO * totalMoney) > ERROR_TORLENCE){
                        totalMoney = MAX(totalMoney, tax);
                        tax = (int)((float)totalMoney / (1.0 + SELL_TAX_RATIO) * SELL_TAX_RATIO + ROUND_VAL);
                        newInvoice.total_money = [NSNumber numberWithInt:totalMoney];
                        newInvoice.tax_money = [NSNumber numberWithInt:tax];
                        newInvoice.sale_money = [NSNumber numberWithInt:totalMoney - tax];
                    }
                }else{
                    newInvoice.tax_money = [NSNumber numberWithInt: (int)((float)[newInvoice.total_money intValue] / (1.0 + SELL_TAX_RATIO) * SELL_TAX_RATIO + ROUND_VAL)];
                    newInvoice.sale_money = [NSNumber numberWithInt:[newInvoice.total_money intValue] - [newInvoice.tax_money intValue]];
                }
                
                // items will be 3 set (name: count: money), error format will start from 11
                const int ITEM_MEMBERS = 3;
                
                int startIndex = (0== (takenQR.count - 12) % ITEM_MEMBERS)? 12: 11;
                if (takenQR.count >= startIndex + ITEM_MEMBERS) {
                    for (int i = startIndex; i<takenQR.count; i=i+ITEM_MEMBERS) {
                        Items *newItem = [NSEntityDescription insertNewObjectForEntityForName:@"Items"
                                                                       inManagedObjectContext:managedObjectContext];
                        
                        if (!newItem) { NSLog(@"Failed to create new item"); }
                        newItem.name = takenQR[i];
                        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                        f.numberStyle = NSNumberFormatterDecimalStyle;
                        if(i+1 < takenQR.count) newItem.count = [f numberFromString:takenQR[i+1]];
                        if(i+2 < takenQR.count) newItem.totalPrice = [f numberFromString:takenQR[i+2]];
                        if(newItem.count != 0) newItem.unitPrice = [NSNumber numberWithFloat: [newItem.totalPrice floatValue] / [newItem.count floatValue]];
                        newItem.belongInvoice = newInvoice;
                        newItem.soldDate = newInvoice.date;
                        if(newInvoice.seller) newItem.companyName = newInvoice.seller.name;
                        
                        NSError *saveErr = nil;
                        if ([managedObjectContext save:&saveErr]) {
                            NSLog(@"[Utility] - save new item");
                        }else{
                            NSLog(@"[Utility] - save item failed");
                        }
                    }
                }
                
                if ([parsedRightQR count] >= 2) {
                    for (int i = 0; i<parsedRightQR.count; i=i+ITEM_MEMBERS) {
                        if( 0 == i % ITEM_MEMBERS && 0 == [parsedRightQR[i] length] ) break;
                        Items *newItem = [NSEntityDescription insertNewObjectForEntityForName:@"Items"
                                                                       inManagedObjectContext:managedObjectContext];
                        
                        if (!newItem) { NSLog(@"Failed to create new item"); }
                        newItem.name = parsedRightQR[i];
                        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                        f.numberStyle = NSNumberFormatterDecimalStyle;
                        if(i+1 < parsedRightQR.count) newItem.count = [f numberFromString:parsedRightQR[i+1]];
                        if(i+2 < parsedRightQR.count) newItem.totalPrice = [f numberFromString:parsedRightQR[i+2]];
                        if(![@"0" isEqualToString: parsedRightQR[i+1]]) {
                            newItem.unitPrice = [NSNumber numberWithFloat: [newItem.totalPrice floatValue] / [newItem.count floatValue]];
                        }else{
                            newItem.unitPrice = newItem.totalPrice;
                        }
                        newItem.belongInvoice = newInvoice;
                        newItem.soldDate = newInvoice.date;
                        if(newInvoice.seller) newItem.companyName = newInvoice.seller.name;
                        
                        NSError *saveErr = nil;
                        if ([managedObjectContext save:&saveErr]) {
                            NSLog(@"[Utility] - save new item");
                        }else{
                            NSLog(@"[Utility] - save item failed");
                        }
                    }
                }
            }else{
                NSLog(@"[Utility] - getInvoiceFromAVMetaDataList: QR contents not enough");
                return E_ERROR_INVALID_QR_CODE;
            }
        }else{
            NSLog(@"[Utility] - scanned objects are not QR codes");
        }
        
        NSError *saveErr = nil;
        if ([managedObjectContext save:&saveErr]) {
            NSLog(@"[ScanViewController] - captureOutput: new invoice was created");
            *invoiceNum = [newInvoice.invoice_num copy];
        }else{
            NSLog(@"[ScanViewController] - captureOutput: fail to save new invoice");
        }
        
    }else{
        NSLog(@"[Utility] - No enough QR metadata to generate Invoice coreData");
    }
    return E_ERROR_FREE;
}

@end
