//
//  Utility.h
//  invoiceQR
//
//  Created by Hank  on 2016/1/17.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Invoice.h"

enum E_ERROR_CODE{
    E_ERROR_FREE = 0,
    E_ERROR_INVALID_QR_CODE,
    E_ERROR_DUPLICATE_INVOICE,
    E_ERROR_ALL_CODES
};

@interface Utility : NSObject
+ (NSMutableArray *)parseInvoiceFromString: (NSString *)invoiceString;
+ (NSMutableArray *)parseRightQRfromString: (NSString *)rightQR;
+ (enum E_ERROR_CODE)getInvoiceFromAVMetaDataList:(NSArray *)invoiceLists
                        save2ManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
                                   withInvoiceNum:(NSString **)invoiceNum;
@end
