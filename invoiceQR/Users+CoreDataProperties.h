//
//  Users+CoreDataProperties.h
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Users.h"

NS_ASSUME_NONNULL_BEGIN

@interface Users (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *branch;
@property (nullable, nonatomic, retain) NSNumber *userID;
@property (nullable, nonatomic, retain) NSString *mail;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSString *vat_num;
@property (nullable, nonatomic, retain) NSDate *lastUpdate;
@property (nullable, nonatomic, retain) NSDate *lastSync;
@property (nullable, nonatomic, retain) Invoice *buy;
@property (nullable, nonatomic, retain) Invoice *sell;

@end

NS_ASSUME_NONNULL_END
