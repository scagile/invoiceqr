//
//  OutputInvoiceTableViewCell.h
//  invoiceQR
//
//  Created by Hank  on 2016/1/18.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OutputInvoiceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyerVAT;
@property (weak, nonatomic) IBOutlet UILabel *sellerVAT;
@property (weak, nonatomic) IBOutlet UILabel *sellMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLabel;

@end
