//
//  SettingTableTableViewController.h
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SettingTableTableViewController : UITableViewController <MFMailComposeViewControllerDelegate, UITextFieldDelegate>

@end
