//
//  lightGreenTheme.m
//  invoiceQR
//
//  Created by Hank  on 2016/4/10.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "lightGreenTheme.h"

@implementation lightGreenTheme

#pragma mark - utility
+ (void)applyNavigationBar: (UINavigationBar *)bar
{
    bar.barTintColor = [self mainColor];
    bar.titleTextAttributes = @{
                                NSForegroundColorAttributeName : [self navigationTitleColor],
                                NSFontAttributeName : [UIFont fontWithName: [self navigationTitleFont]
                                                                      size: self.navigationTitleFontSize]
                                };
}

#pragma mark - configuration
+ (UIColor *)mainColor
{
    return [UIColor colorWithRed:28.0/255 green:158.0/255 blue:121.0/255 alpha:1.0f];
}

+ (UIColor *)darkColor
{
    //return [UIColor colorWithRed:7.0/255 green:61.0/255 blue:48.0/255 alpha:1.0f];
    return [UIColor blackColor];
}

+ (NSString *)fontName
{
    return @"Avenir-Book";
}

+ (NSString *)boldFontName
{
    return @"Avenir-Black";
}

#pragma mark - getter & setting
+ (UIColor *)viewBackgroundColor
{
    return [self mainColor];
}

+ (UIColor *)viewTintColor
{
    return [UIColor whiteColor];
}

+ (NSString *) navigationTitleFont
{
    return [self boldFontName];
}

+ (UIColor *)navigationTitleColor
{
    return [UIColor whiteColor];
}

+ (float)navigationTitleFontSize
{
    return 18.0f;
}

+ (UIColor *)navigationBarBarTintColor
{
    return [self mainColor];
}

+ (UIColor *)navigationForegroundColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)naviagtionBackgroundColor
{
    return [UIColor blackColor];
}

+ (UIColor *)navigationBarTintColor
{
    return [UIColor whiteColor];
}

+ (NSString *)labelFont
{
    return [self fontName];
}

+ (float)labelFontSize
{
    return 16.0f;
}

+ (UIColor *)buttonBackgroundColor
{
    return [self darkColor];
}

+ (UIColor *)buttonTitleColor
{
    return [UIColor whiteColor];
}

+ (float)buttonCornerRadius
{
    return 3.0f;
}

+ (NSString *)buttonFont
{
    return [self boldFontName];
}

@end
