//
//  Items+CoreDataProperties.m
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Items+CoreDataProperties.h"

@implementation Items (CoreDataProperties)

@dynamic count;
@dynamic name;
@dynamic totalPrice;
@dynamic unitPrice;
@dynamic soldDate;
@dynamic companyName;
@dynamic date;
@dynamic belongInvoice;

@end
