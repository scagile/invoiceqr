//
//  Invoice+CoreDataProperties.h
//  invoiceQR
//
//  Created by Hank  on 2016/4/24.
//  Copyright © 2016年 SCAgile. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Invoice.h"

NS_ASSUME_NONNULL_BEGIN

@interface Invoice (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *buy_vat_no;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSNumber *index;
@property (nullable, nonatomic, retain) NSString *invoice_num;
@property (nullable, nonatomic, retain) NSDate *lastUpdate;
@property (nullable, nonatomic, retain) NSNumber *qr_items_count;
@property (nullable, nonatomic, retain) NSNumber *sale_money;
@property (nullable, nonatomic, retain) NSString *sell_vat_no;
@property (nullable, nonatomic, retain) NSNumber *tax_money;
@property (nullable, nonatomic, retain) NSNumber *total_items_count;
@property (nullable, nonatomic, retain) NSNumber *total_money;
@property (nullable, nonatomic, retain) NSString *randomNum;
@property (nullable, nonatomic, retain) NSString *encryp;
@property (nullable, nonatomic, retain) NSString *period;
@property (nullable, nonatomic, retain) Users *buyer;
@property (nullable, nonatomic, retain) NSSet<Items *> *items;
@property (nullable, nonatomic, retain) Users *seller;

@end

@interface Invoice (CoreDataGeneratedAccessors)

- (void)addItemsObject:(Items *)value;
- (void)removeItemsObject:(Items *)value;
- (void)addItems:(NSSet<Items *> *)values;
- (void)removeItems:(NSSet<Items *> *)values;

@end

NS_ASSUME_NONNULL_END
