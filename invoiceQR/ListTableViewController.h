//
//  ListTableViewController.h
//  invoiceQR
//
//  Created by Hank  on 2016/1/16.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreData;

@interface ListTableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate>

@property (nonatomic, strong) NSMutableArray *invoiceLists;

@end
