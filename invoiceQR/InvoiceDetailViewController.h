//
//  InvoiceDetailViewController.h
//  invoiceQR
//
//  Created by Hank  on 2016/2/12.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Invoice.h"
@import CoreData;

@interface InvoiceDetailViewController : UIViewController<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSManagedObjectID *invoiceID;
@property (strong, nonatomic) NSString *invoiceNum;
@end
