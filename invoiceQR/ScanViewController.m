//
//  ScanViewController.m
//  invoiceQR
//
//  Created by Hank  on 2015/11/18.
//  Copyright © 2015年 SCAgile. All rights reserved.
//

@import AVFoundation;
#import "ScanViewController.h"
#import "ListTableViewController.h"
#import "Constants.h"
#import "Invoice.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "UIView+Toast.h"
#import "lightGreenTheme.h"

@interface ScanViewController () <AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, strong) CALayer *arLayer;

@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, strong) NSMutableArray *detectedObjs;

@property (nonatomic, weak) NSManagedObjectContext *managedObjectContext;
@end

@implementation ScanViewController

#pragma mark - getter & setter
- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext = appDelegate.managedObjectContext;
    }
    return _managedObjectContext;
}

- (NSMutableArray *)detectedObjs
{
    if (!_detectedObjs) {
        _detectedObjs = [[NSMutableArray alloc] init];
    }
    return _detectedObjs;
}

- (AVCaptureSession *)captureSession
{
    if (!_captureSession) {
        NSError *error = nil;
        
        // back camera
        AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if (captureDevice.isAutoFocusRangeRestrictionSupported) {
            if ([captureDevice lockForConfiguration: &error]) {
                captureDevice.autoFocusRangeRestriction = AVCaptureAutoFocusRangeRestrictionNear;
                [captureDevice unlockForConfiguration];
            }
        }
        
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error: &error];
        if (!input) {
            NSLog(@"%@", [error localizedDescription]);
        }else{
            _captureSession = [[AVCaptureSession alloc] init];
            if ([_captureSession canAddInput:input]) {
                [_captureSession addInput: input];
                
                [self.previewLayer setVideoGravity: AVLayerVideoGravityResizeAspectFill];
                self.previewLayer.frame = self.view.bounds;
            }else{
                NSLog(@"error to add input to session");
            }
            
            AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
            if ([_captureSession canAddOutput:captureMetadataOutput]) {
                [_captureSession addOutput:captureMetadataOutput];
                /*
                 dispatch_queue_t dispatchQueue = dispatch_queue_create("scanQueue", NULL);
                 [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
                 */
                [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
                [captureMetadataOutput setMetadataObjectTypes: [NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
            }else{
                NSLog(@"error to add output to session");
            }
            
            //[self.captureSession startRunning];

        }
    }
    return _captureSession;
}

- (AVCaptureVideoPreviewLayer *)previewLayer
{
    if (!_previewLayer) {
        if (self.captureSession) {
            _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];

            [self.view.layer addSublayer:self.previewLayer];
            
            self.arLayer = [CALayer layer];
            self.arLayer.frame = self.view.bounds;
            [self.view.layer addSublayer:self.arLayer];
            
#if 1
            const float scale = 0.1f;
            const int focusViewWidth = self.view.bounds.size.width * scale;
            const int focusViewHeight = self.view.bounds.size.height * scale;
            UIImageView *focusLeftUpView = [[UIImageView alloc] initWithFrame: CGRectMake(self.view.bounds.size.width *0.05f, self.view.bounds.size.height * 0.4f - focusViewHeight, focusViewWidth, focusViewHeight)];
            focusLeftUpView.image = [UIImage imageNamed:@"focusLeftUp.png"];
            [self.view addSubview:focusLeftUpView];
            
            UIImageView *focusLeftLowView = [[UIImageView alloc] initWithFrame: CGRectMake(self.view.bounds.size.width *0.05f, self.view.bounds.size.height * 0.6f, focusViewWidth, focusViewHeight)];
            focusLeftLowView.image = [UIImage imageNamed:@"focusLeftLow.png"];
            [self.view addSubview:focusLeftLowView];
            
            UIImageView *focusRightUpView = [[UIImageView alloc] initWithFrame: CGRectMake(self.view.bounds.size.width *0.95f - focusViewWidth, self.view.bounds.size.height * 0.4f - focusViewHeight, focusViewWidth, focusViewHeight)];
            focusRightUpView.image = [UIImage imageNamed:@"focusRightUp.png"];
            [self.view addSubview:focusRightUpView];
            
            UIImageView *focusRightLowView = [[UIImageView alloc] initWithFrame: CGRectMake(self.view.bounds.size.width *0.95f - focusViewWidth, self.view.bounds.size.height * 0.6f, focusViewWidth, focusViewHeight)];
            focusRightLowView.image = [UIImage imageNamed:@"focusRightLow.png"];
            [self.view addSubview:focusRightLowView];
            
#else // draw by our own
            UIView *boxView = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width * 0.2f, self.view.bounds.size.height * 0.2f, self.view.bounds.size.width - self.view.bounds.size.width * 0.4f, self.view.bounds.size.height - self.view.bounds.size.height * 0.4f)];
            boxView.layer.borderColor = [UIColor greenColor].CGColor;
            boxView.layer.borderWidth = 1.0f;
            [self.view addSubview:boxView];
#endif
        }
    }
    return _previewLayer;
}

- (AVAudioPlayer *)audioPlayer
{
    if (!_audioPlayer) {
        NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
        NSURL *beepURL = [NSURL URLWithString:beepFilePath];
        NSError *error;
        
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
        if (error) {
            NSLog(@"Could not play beep file.");
            NSLog(@"%@", [error localizedDescription]);
        }else{
            [_audioPlayer prepareToPlay];
        }
    }
    return _audioPlayer;
}

#pragma mark - view controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //self.starting = false;
    [self initInterface];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    [self startScan];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self stopScan];
    [self turnTorchOn:NO];
}

- (void)applicationDidEnterBackground: (NSNotification *)notification
{
    [self stopScan];
}

- (void)applicationWillEnterForeground: (NSNotification *)notification
{
    [self startScan];
}

#pragma mark - init
- (void)initInterface
{
    //[[UIView appearanceWhenContainedInInstancesOfClasses:@[[UITabBar class]]] setTintColor:[lightGreenTheme naviagtionBackgroundColor]];
}

#pragma mark - delegation
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    [self.detectedObjs removeAllObjects];

    // note, you need to transform on each element
    for (AVMetadataObject *metadata in metadataObjects) {
        AVMetadataObject *transformedObject = [self.previewLayer transformedMetadataObjectForMetadataObject:metadata];
        [self.detectedObjs addObject:transformedObject];
    }
    
    NSArray *sublayers = [[self.arLayer sublayers] copy];
    for (CALayer *sublayer in sublayers) {
        [sublayer removeFromSuperlayer];
    }
    
    for (AVMetadataObject *metadata in self.detectedObjs){
        if ([metadata isKindOfClass: [AVMetadataMachineReadableCodeObject class]]) {
            // [metadata type] isEqualToString:AVMetadataObjectTypeQRCode]
            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
            shapeLayer.strokeColor = [UIColor redColor].CGColor;
            shapeLayer.fillColor = [UIColor clearColor].CGColor;
            shapeLayer.lineWidth = 2.0;
            shapeLayer.lineJoin = kCALineJoinRound;
        
            // direct assign? shapeLayer = createPath?
            CGPathRef path = createPathForPoints([(AVMetadataMachineReadableCodeObject *)metadata corners]);
            shapeLayer.path = path;
            CFRelease(path);
            [self.arLayer addSublayer:shapeLayer];
        }
    }
    
    const int PAIR_COUNT = 2;
    if (metadataObjects.count >= PAIR_COUNT) {

        NSString *invoiceNum;
        enum E_ERROR_CODE error =
            [Utility getInvoiceFromAVMetaDataList:self.detectedObjs
                        save2ManagedObjectContext:self.managedObjectContext
                                    withInvoiceNum: &invoiceNum];

        NSArray *sublayers = [[self.arLayer sublayers] copy];
        [self.detectedObjs removeAllObjects];
        for (CALayer *sublayer in sublayers) {
            [sublayer removeFromSuperlayer];
        }
        
        BOOL isContinueScan = [[NSUserDefaults standardUserDefaults] boolForKey: KEY_NAME[KEY_ENABLE_CONTINUE_SCAN]];
        
        switch(error){
            case E_ERROR_DUPLICATE_INVOICE:
                [self scanErrorWithMessage:[NSString stringWithFormat:@"Invoice %@ is duplicate", invoiceNum] withTransistion: !isContinueScan];
                break;
            case E_ERROR_INVALID_QR_CODE:
                [self scanErrorWithMessage:@"Invalid QR codes" withTransistion:NO];
                break;
            case E_ERROR_FREE:
            default:
                    [self scanErrorWithMessage:[NSString stringWithFormat:@"Invoice %@ has been added", invoiceNum] withTransistion: !isContinueScan];
                break;
        }
    }
}

#pragma mark - utility
- (BOOL)startScan
{
    self.detectedObjs = nil;
    [self.captureSession startRunning];
    return YES;
}

- (void)stopScan
{
    [self.captureSession stopRunning];
    self.captureSession = nil;
    
    [self.previewLayer removeFromSuperlayer];
    self.previewLayer = nil;
    
    [self.arLayer removeFromSuperlayer];
    self.arLayer = nil;
}

- (void)scanErrorWithMessage: (NSString *)errorMsg
             withTransistion: (BOOL)isTransist
{
    if (self.audioPlayer) [self.audioPlayer play];
    
    [self.captureSession stopRunning];
    const double DELAY = 1.0;
    [self.view makeToast: errorMsg
                duration: DELAY
                position:CSToastPositionCenter];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(DELAY * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.captureSession startRunning];
        if(isTransist){
            self.tabBarController.selectedIndex = LIST_TAB;
        }
    });
}

CGMutablePathRef createPathForPoints(NSArray* points)
{
    CGMutablePathRef path = CGPathCreateMutable();
    CGPoint point;
    
    if ([points count] > 0)
    {
        CGPointMakeWithDictionaryRepresentation((CFDictionaryRef)[points objectAtIndex:0], &point);
        CGPathMoveToPoint(path, nil, point.x, point.y);
        
        int i = 1;
        while (i < [points count])
        {
            CGPointMakeWithDictionaryRepresentation((CFDictionaryRef)[points objectAtIndex:i], &point);
            CGPathAddLineToPoint(path, nil, point.x, point.y);
            i++;
        }
        
        CGPathCloseSubpath(path);
    }
    
    return path;
}

- (IBAction)turnOnLight:(UIBarButtonItem *)sender {
    // UIBarButtonItem selected mechanism
    // ref: http://stackoverflow.com/questions/8267758/programmatically-highlight-uibarbuttonitem
    sender.style = (sender.style == UIBarButtonItemStylePlain)? UIBarButtonItemStyleDone : UIBarButtonItemStylePlain;
    if(sender.style == UIBarButtonItemStyleDone){
        // turn on the flash light
        [self turnTorchOn:YES];
    }else{
        [self turnTorchOn:NO];
    }
}

- (void)turnTorchOn:(bool)on
{
    // ref: http://www.mobile-open.com/2015/92703.html
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        if ([device hasTorch] && [device hasFlash]){
            
            [device lockForConfiguration:nil];
            if (on) {
                [device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
                
            } else {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
            }
            [device unlockForConfiguration];
        }
    }
}

#pragma mark - navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // switch to table view by tabs
}

@end
