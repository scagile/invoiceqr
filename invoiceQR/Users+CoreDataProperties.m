//
//  Users+CoreDataProperties.m
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Users+CoreDataProperties.h"

@implementation Users (CoreDataProperties)

@dynamic branch;
@dynamic userID;
@dynamic mail;
@dynamic name;
@dynamic type;
@dynamic vat_num;
@dynamic lastUpdate;
@dynamic lastSync;
@dynamic buy;
@dynamic sell;

@end
