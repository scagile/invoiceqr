//
//  SettingTableTableViewController.m
//  invoiceQR
//
//  Created by Hank  on 2016/3/26.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "SettingTableTableViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#include "lightGreenTheme.h"

@interface SettingTableTableViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *entranceSegmentControl;
@property (weak, nonatomic) IBOutlet UISwitch *enableTutorialSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *enableContinueScanSwitch;

@property (weak, nonatomic) IBOutlet UITextField *VATfilterTextField;
@property (strong, nonatomic) NSString *prevVAT;
@end

@implementation SettingTableTableViewController

#pragma mark - view controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initInterface];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    self.enableTutorialSwitch.on = [userDefaults boolForKey:KEY_NAME[KEY_ENABLE_TUTORIAL]];
    self.enableContinueScanSwitch.on = [userDefaults boolForKey:KEY_NAME[KEY_ENABLE_CONTINUE_SCAN]];
    self.entranceSegmentControl.selectedSegmentIndex = [userDefaults integerForKey: KEY_NAME[KEY_ENTRANCE]];
    
    self.VATfilterTextField.delegate = self;
    
    [self.VATfilterTextField addTarget:self action:@selector(disableTextFieldAfterEdited) forControlEvents:UIControlEventEditingDidEndOnExit];
    //UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disableTextFieldAfterEdited)];
    //[self.view addGestureRecognizer:tapRecognizer];
}

- (void)disableTextFieldAfterEdited
{
    [self.VATfilterTextField endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
}

- (void)viewWillAppear:(BOOL)animated
{
    self.entranceSegmentControl.apportionsSegmentWidthsByContent = YES;
    [self.entranceSegmentControl setNeedsLayout];
}

#pragma mark - data source & delegation - Table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(0 == section){
        return 3;
    }else if(1 == section){
        return 3;
    }
    return 0;
}

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
    NSIndexPath *feedbackIndexPath = [NSIndexPath indexPathForRow:2 inSection:1];
    
    if (indexPath == feedbackIndexPath) {
        if([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
            mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
            
            [mailCont setSubject: NSLocalizedString(@"[InvoiceQR] Feedback: ", @"feedback mail title")];
            [mailCont setToRecipients:[NSArray arrayWithObject:@"scagile.tech@gmail.com"]];
            [mailCont setMessageBody:@"" isHTML:NO];
            
            [self presentViewController:mailCont animated:YES completion:nil];
        }
    }
}

// - (void) tableView: (UITableView *) tableView accessoryButtonTappedForRowWithIndexPath: (NSIndexPath *) indexPath{ ... }

#pragma mark - MFMail delegation
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - text field delegation
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.prevVAT = [NSString stringWithString:textField.text];
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    const int VAT_LENGTH = 8;
    if ( VAT_LENGTH != [textField.text length] ) {
        textField.text = self.prevVAT;
    }
}

#pragma mark - button actions
- (IBAction)switchChanged:(UISwitch *)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool: self.enableTutorialSwitch.isOn
                   forKey: KEY_NAME[KEY_ENABLE_TUTORIAL]];
    [userDefaults synchronize];
}

- (IBAction)entranceChanged:(UISegmentedControl *)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:sender.selectedSegmentIndex
                      forKey:KEY_NAME[KEY_ENTRANCE]];
    [userDefaults synchronize];
}

- (IBAction)continueScanSwitchChange:(UISwitch *)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool: self.enableContinueScanSwitch.isOn
                   forKey: KEY_NAME[KEY_ENABLE_CONTINUE_SCAN]];
    [userDefaults synchronize];
}

#pragma mark - helper function
- (void)initInterface
{
    //[[UIView appearanceWhenContainedInInstancesOfClasses:@[[UITabBar class]]] setTintColor:[lightGreenTheme naviagtionBackgroundColor]];
#if 0
    for(UITabBarItem *tabItem in self.tabBarController.tabBar.items){
        tabItem.image = [tabItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        tabItem.selectedImage = [tabItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
#endif
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier" forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
