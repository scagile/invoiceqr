//
//  ListItemTableViewCell.m
//  invoiceQR
//
//  Created by Hank  on 2016/3/30.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "ListItemTableViewCell.h"
#import "lightGreenTheme.h"

@implementation ListItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.nameLabel.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                          size:[lightGreenTheme labelFontSize]];
    self.unitPriceLabel.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                          size:[lightGreenTheme labelFontSize]];
    self.countLabel.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                          size:[lightGreenTheme labelFontSize]];
    self.totalPriceLabel.font = [UIFont fontWithName:[lightGreenTheme labelFont]
                                          size:[lightGreenTheme labelFontSize]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
