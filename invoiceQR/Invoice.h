//
//  Invoice.h
//  invoiceQR
//
//  Created by Hank  on 2016/3/24.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Items, Users;

NS_ASSUME_NONNULL_BEGIN

@interface Invoice : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Invoice+CoreDataProperties.h"
