//
//  Constants.h
//  invoiceQR
//
//  Created by Hank  on 2016/1/16.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

enum TABS
{
    SCAN_TAB = 0,
    LIST_TAB,
    SETTING_TAB,
    ALL_VCs
};

enum PROGRAM_ENTRY
{
    ENTRY_TUTORIAL=0,
    ENTRY_QR,
    ENTRY_ALL
};

enum E_INVOICE_TAGS
{
    INVOICE_NUM=0,
    INVOICE_DATE,
    INVOICE_RANDOM,
    INVOICE_SALE_MONEY_HEX,
    INVOICE_TOTAL_MOENY_HEX,
    INVOICE_BUY_VAT_NO,
    INVOICE_SELL_VAT_NO,
    INVOICE_ENCRY,
    INVOICE_OTHER_INFO,
    INVOICE_QR_ITEMS,
    INVOICE_TOTAL_ITEMS,
    INVOICE_ENCODE,
    INVOICE_ITEMS,
    INVOICE_ALL_TAGS
};

enum E_ENCODING
{
    ENCODE_BIG5 = 0,
    ENCODE_UTF8,
    ENCODE_BASE64
};

enum E_KEY
{
    KEY_ENABLE_TUTORIAL=0,
    KEY_ENABLE_CONTINUE_SCAN,
    KEY_ENTRANCE,
    KEY_ALL
};

extern NSString *const PROGRAM_ENTRY_NAME[ENTRY_ALL];
extern NSString *const KEY_NAME[KEY_ALL];

#endif /* Constants_h */
