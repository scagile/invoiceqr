//
//  lightGreenTheme.h
//  invoiceQR
//
//  Created by Hank  on 2016/4/10.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface lightGreenTheme : NSObject
+ (UIColor *)viewBackgroundColor;
+ (UIColor *)viewTintColor;

+ (UIColor *)navigationBarBarTintColor;
+ (UIColor *)navigationForegroundColor;
+ (UIColor *)naviagtionBackgroundColor;
+ (NSString *)navigationTitleFont;
+ (float)navigationTitleFontSize;
+ (UIColor *)navigationBarTintColor;

+ (NSString *)labelFont;
+ (float)labelFontSize;

+ (UIColor *)buttonBackgroundColor;
+ (UIColor *)buttonTitleColor;
+ (float)buttonCornerRadius;
+ (NSString *)buttonFont;
@end
