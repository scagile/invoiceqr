//
//  InvoiceDetailViewController.m
//  invoiceQR
//
//  Created by Hank  on 2016/2/12.
//  Copyright © 2016年 SCAgile. All rights reserved.
//

#import "InvoiceDetailViewController.h"
#import "Items.h"
#import "Users.h"
#import "AppDelegate.h"
#import "ListItemTableViewCell.h"
//#import "AFNetworking.h"

@interface InvoiceDetailViewController ()
@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *invoiceFectchResultController;

@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthIntervalLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalValLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellerVATLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyerVATLabel;
@property (weak, nonatomic) IBOutlet UITableView *itemTable;
@end

@implementation InvoiceDetailViewController

#pragma mark - view controller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initInterface];
    [self fetchItems];
}

- (void)viewWillAppear:(BOOL)animated
{
    // nil: edit mode
    if (self.invoiceID != nil) {
        NSError *error = nil;
        Invoice *invoice = (Invoice *)[self.managedObjectContext existingObjectWithID:self.invoiceID
                                                                                error:&error];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy/MM/dd"];
        self.dateLabel.text = [formatter stringFromDate:invoice.date];
        if(invoice.seller) self.companyLabel.text = invoice.seller.name;

        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
        
        NSCalendar *gregorian = [NSCalendar currentCalendar];
        NSDateComponents *comps = [gregorian components:unitFlags fromDate: invoice.date];
        [comps setYear:[comps year] - 1911];
        NSDate *taiwanYear = [gregorian dateFromComponents:comps];
        
        [formatter setDateFormat:@"yyy"];
        self.yearLabel.text = [formatter stringFromDate:taiwanYear];
        
        [formatter setDateFormat:@"MM"];
        int month = [[formatter stringFromDate:taiwanYear] intValue];
        int toMonth = (int)((month+1)/2)*2;
        int fromMonth = toMonth - 1;
        self.monthIntervalLabel.text = [NSString stringWithFormat:@"%02d-%02d", fromMonth, toMonth];
                
        NSMutableString *invoiceStr = [NSMutableString stringWithString:invoice.invoice_num];
        [invoiceStr insertString:@"-" atIndex:2];
        self.invoiceNumLabel.text = [invoiceStr copy];
        
        self.totalValLabel.text = [invoice.total_money stringValue];
        
        self.sellerVATLabel.text = invoice.sell_vat_no;
        self.buyerVATLabel.text = invoice.buy_vat_no;
    }
}

#pragma mark - delegation
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.invoiceFectchResultController.sections objectAtIndex:0];
    return [sectionInfo numberOfObjects];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ListItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"INVOICE_ITEM_CELL"];
    if (cell == nil) {
        cell = [[ListItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"INVOICE_ITEM_CELL"];
    }
    
    cell.nameLabel.text = @"品名";
    cell.countLabel.text = @"數量";
    cell.unitPriceLabel.text = @"單價";
    cell.totalPriceLabel.text = @"總價";
    cell.backgroundColor = [UIColor lightGrayColor];
    
    // lock as a table header
    self.itemTable.tableHeaderView = cell;
    self.itemTable.contentInset = UIEdgeInsetsMake(cell.frame.size.height, 0, 0, 0);

    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"INVOICE_ITEM_CELL";
    
    ListItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ListItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Items *item = [self.invoiceFectchResultController objectAtIndexPath:indexPath];
    cell.nameLabel.text = item.name;
    cell.countLabel.text = [item.count stringValue];
    cell.unitPriceLabel.text = [item.unitPrice stringValue];
    cell.totalPriceLabel.text = [item.totalPrice stringValue];
    
    return cell;
}

#pragma mark - init
- (void)initInterface
{
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - setter & getter
- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext = appDelegate.managedObjectContext;
    }
    return _managedObjectContext;
}

#pragma mark - utility

- (void)fetchItems
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Items"
                                              inManagedObjectContext:self.managedObjectContext];
    NSSortDescriptor *nameSortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"belongInvoice.invoice_num" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:nameSortDescriptor, nil];
    fetchRequest.sortDescriptors = sortDescriptors;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"belongInvoice.invoice_num contains %@", self.invoiceNum];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    
    self.invoiceFectchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                             managedObjectContext:self.managedObjectContext
                                                                               sectionNameKeyPath:nil
                                                                                        cacheName:nil];
    
    self.invoiceFectchResultController.delegate = self;
    
    NSError *fetchError = nil;
    if ([self.invoiceFectchResultController performFetch:&fetchError]) {
        NSLog(@"Successfully fetched data from invoce entity");
    }else{
        NSLog(@"Failed to fetch any data from invoice entity");
    }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Invoice" inManagedObjectContext:self.managedObjectContext];
    predicate = [NSPredicate predicateWithFormat:@"invoice_num contains %@", self.invoiceNum];
    request.predicate = predicate;
    NSError *error = nil;
    NSArray *objs = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no invoice find" format:@"%@", [error localizedDescription]];
    }
    if (objs.count > 0) {
        Invoice *invoice = [objs objectAtIndex:0];
        if(!invoice.seller){
            request.entity = [NSEntityDescription entityForName:@"Users" inManagedObjectContext:self.managedObjectContext];
            predicate = [NSPredicate predicateWithFormat:@"vat_num contains %@", invoice.invoice_num];
            request.predicate = predicate;
            objs = [self.managedObjectContext executeFetchRequest:request error:&error];
            if(objs.count > 0) invoice.seller = [objs objectAtIndex:0];
            NSError *saveErr = nil;
            if ([self.managedObjectContext save:&saveErr]) {
                NSLog(@"[InvoiceDetail] - link invoice with company successfully");
            }else{
                NSLog(@"[InvoiceDetail] - link invoice with company failed");
            }
        }
    }
}

- (IBAction)deleteInvoice:(UIButton *)sender
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete Confirm"
                                  message:@"Are you sure you want to delete this invoice?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Delete"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    if(self.invoiceID != nil){
                                        NSError *error = nil;
                                        Invoice *invoice = (Invoice *)[self.managedObjectContext existingObjectWithID:self.invoiceID
                                                                                                                error:&error];
                                        if (invoice == nil){
                                            NSLog(@"Failed to get the invoice ");
                                            return;
                                        }
                                        
                                        [self.managedObjectContext deleteObject:invoice];
                                        if ([self.managedObjectContext save:&error]){
                                            NSLog(@"invoice was deleted");
                                        }else {
                                            NSLog(@"Failed to delete the invoice, Error = %@", error);
                                        }
                                    }
                                    [self.navigationController popViewControllerAnimated:YES];
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //Handel no, thanks button
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
@end
