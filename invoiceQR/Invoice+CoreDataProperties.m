//
//  Invoice+CoreDataProperties.m
//  invoiceQR
//
//  Created by Hank  on 2016/4/24.
//  Copyright © 2016年 SCAgile. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Invoice+CoreDataProperties.h"

@implementation Invoice (CoreDataProperties)

@dynamic buy_vat_no;
@dynamic date;
@dynamic index;
@dynamic invoice_num;
@dynamic lastUpdate;
@dynamic qr_items_count;
@dynamic sale_money;
@dynamic sell_vat_no;
@dynamic tax_money;
@dynamic total_items_count;
@dynamic total_money;
@dynamic randomNum;
@dynamic encryp;
@dynamic period;
@dynamic buyer;
@dynamic items;
@dynamic seller;

@end
